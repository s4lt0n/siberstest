﻿using Sibers.BussinessLogic.DTO;
using Sibers.BussinessLogic.Interfaces;
using Sibers.DAL.Interfaces;
using Sibers.DAL.Models;
using Sibers.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sibers.BussinessLogic.Mapping
{
    public class ProjectMap : IProjectMap
    {
        private IUnitOfWork Database { get; set; }

        public ProjectMap (IUnitOfWork uow)
        {
            Database = uow;
        }

        public Project GetProjectDAL (ProjectDTO projectDTO)
        {
            var newProject = new Project
            {
                Name = projectDTO.Name,
                CustomerCompanyName = projectDTO.CustomerCompanyName,
                ExecutingCompanyName = projectDTO.ExecutingCompanyName,
                StartDate = projectDTO.StartDate,
                EndDate = projectDTO.EndDate,
                ProjectChief = Database.Employers.Get(projectDTO.ProjectChiefId),
                Priority = projectDTO.Priority,
                Comment = projectDTO.Comment
            };

            return newProject;
        }

        public ProjectDTO GetProjectDTO (Project projectDAL)
        {
            var a = projectDAL.ProjectChief;
            var projectDTO = new ProjectDTO
            {
                Id = projectDAL.Id,
                Name = projectDAL.Name,
                CustomerCompanyName = projectDAL.CustomerCompanyName,
                ExecutingCompanyName = projectDAL.ExecutingCompanyName,
                ProjectChiefId = projectDAL.ProjectChief.Id,
                StartDate = projectDAL.StartDate,
                EndDate = projectDAL.EndDate,
                ProjectWorkersId = projectDAL.ProjectWorkers.Select(x => x.Id),
                Comment = projectDAL.Comment,
                Priority = projectDAL.Priority
            };

            return projectDTO;
        }
    }
}