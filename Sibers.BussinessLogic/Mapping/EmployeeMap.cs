﻿using Sibers.BussinessLogic.DTO;
using Sibers.BussinessLogic.Interfaces;
using Sibers.DAL.Interfaces;
using Sibers.DAL.Models;
using System;

namespace Sibers.BussinessLogic.Mapping
{
    public class EmployeeMap : IEmployeeMap
    {
        private IUnitOfWork Database { get; set; }

        private Exception employeeNotFound = new Exception("Работник не найден");

        public EmployeeMap (IUnitOfWork uow)
        {
            Database = uow;
        }

        public Employee GetEmployeeDAL (EmployeeDTO employeeDTO)
        {
            if (employeeDTO == null)
            {
                throw employeeNotFound;
            }

            var employeeDAL = new Employee
            {
                FirstName = employeeDTO.FirstName,
                LastName = employeeDTO.LastName,
                Patronymic = employeeDTO.Patronymic,
                EMail = employeeDTO.EMail
            };

            return employeeDAL;
        }

        public EmployeeDTO GetEmployeeDTO (Employee employeeDAL)
        {

            if (employeeDAL == null)
            {
                return null;
            }

            var employee = Database.Employers.Get(employeeDAL.Id);
            if (employee == null)
            {
                return null;
            }

            var employeeDTO = new EmployeeDTO
            {
                Id = employeeDAL.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Patronymic = employee.Patronymic,
                EMail = employee.EMail
            };

            return employeeDTO;
        }
    }
}