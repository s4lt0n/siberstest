﻿using Sibers.BussinessLogic.DTO;
using Sibers.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sibers.BussinessLogic.Interfaces
{
    public interface IEmployeeMap
    {
        Employee GetEmployeeDAL(EmployeeDTO employeeDTO);
        EmployeeDTO GetEmployeeDTO (Employee employeeDAL);
    }
}
