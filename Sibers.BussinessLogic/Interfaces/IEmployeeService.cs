﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sibers.BussinessLogic.DTO;
using Sibers.DAL.Models;

namespace Sibers.BussinessLogic.Interfaces
{
    public interface IEmployeeService : IEntityService<EmployeeDTO>, IDisposable
    {
    }
}
