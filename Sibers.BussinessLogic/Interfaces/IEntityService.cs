﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sibers.BussinessLogic.Interfaces
{
    /// <summary>
    /// Общий интерфейс для CRUD операций над сущностью
    /// </summary>
    /// <typeparam name="T">Сущность</typeparam>
    public interface IEntityService<T>
    {
        T Create (T Entity);
        T Update (T Entity);
        void Delete (T Entity);
        IEnumerable<T> GetAll ();
        T Get (int id);
    }
}
