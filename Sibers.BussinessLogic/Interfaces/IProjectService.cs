﻿using Sibers.BussinessLogic.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sibers.DAL.Models;

namespace Sibers.BussinessLogic.Interfaces
{
    public interface IProjectService : IEntityService<ProjectDTO>, IDisposable
    {
        void SetChief(ProjectDTO projectDTO, EmployeeDTO employeeDTO);
        void AddWorker(ProjectDTO projectDTO, EmployeeDTO employeeDTO);
        void DeleteWorker(ProjectDTO projectDTO, EmployeeDTO employeeDTO);
    }
}
