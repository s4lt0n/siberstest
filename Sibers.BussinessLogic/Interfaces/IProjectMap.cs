﻿using Sibers.BussinessLogic.DTO;
using Sibers.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sibers.BussinessLogic.Interfaces
{
    public interface IProjectMap
    {
        Project GetProjectDAL (ProjectDTO projectDTO);
        ProjectDTO GetProjectDTO (Project projectDAL);
    }
}
