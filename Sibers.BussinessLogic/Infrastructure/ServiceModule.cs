﻿using Ninject.Modules;
using Sibers.BussinessLogic.DTO;
using Sibers.BussinessLogic.Interfaces;
using Sibers.BussinessLogic.Mapping;
using Sibers.BussinessLogic.Services;
using Sibers.DAL.Interfaces;
using Sibers.DAL.Repositories;

namespace Sibers.BussinessLogic.Infrastructure
{
    /// <summary>
    /// Класс для регистрации реализаций интерфейсов
    /// </summary>
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<EFUnitOfWork>();
            Bind<IProjectService>().To<ProjectService>();
            Bind<IEmployeeService>().To<EmployeeService>();
            Bind<IProjectMap>().To<ProjectMap>();
            Bind<IEmployeeMap>().To<EmployeeMap>();
        }
    }
}