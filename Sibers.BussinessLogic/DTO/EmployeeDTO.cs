﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sibers.BussinessLogic.DTO
{
    public class EmployeeDTO
    {
        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronymic { get; set; }

        /// <summary>
        /// E-Mail
        /// </summary>
        public string EMail { get; set; }

        public string FullName => $"{LastName} {FirstName} {Patronymic}";
    }
}