﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sibers.BussinessLogic.DTO;
using Sibers.BussinessLogic.Interfaces;
using Sibers.DAL.Interfaces;

namespace Sibers.BussinessLogic.Services
{
    public class EmployeeService : IEmployeeService
    {
        private IUnitOfWork Database { get; set; }
        private IProjectMap ProjectMap { get; set; }
        private IEmployeeMap EmployeeMap { get; set; }

        public EmployeeService (IUnitOfWork uow, IProjectMap pm, IEmployeeMap em)
        {
            Database = uow;
            ProjectMap = pm;
            EmployeeMap = em;
        }

        public EmployeeDTO Create (EmployeeDTO Entity)
        {
            var newEmployee = EmployeeMap.GetEmployeeDAL(Entity);

            var newEmployeeDAL = Database.Employers.Create(newEmployee);
            Database.Save();
            return EmployeeMap.GetEmployeeDTO(newEmployeeDAL);
        }

        public EmployeeDTO Update (EmployeeDTO Entity)
        {

            var employeeForUpdate = Database.Employers.Get(Entity.Id);

            employeeForUpdate.FirstName = Entity.FirstName;

            employeeForUpdate.LastName = Entity.LastName;
            employeeForUpdate.Patronymic = Entity.Patronymic;
            employeeForUpdate.EMail = Entity.EMail;

            var updatedEmployeeDAL = Database.Employers.Update(employeeForUpdate);
            Database.Save();

            return EmployeeMap.GetEmployeeDTO(updatedEmployeeDAL);
        }

        public void Delete (EmployeeDTO Entity)
        {
            Database.Employers.Delete(Entity.Id);
            Database.Save();
        }

        public IEnumerable<EmployeeDTO> GetAll ()
        {
            return Database.Employers.GetAll().Select(x => (EmployeeMap.GetEmployeeDTO(x)));
        }

        public void Dispose ()
        {
            Database.Dispose();
        }

        public EmployeeDTO Get (int id)
        {
            return EmployeeMap.GetEmployeeDTO(Database.Employers.Get(id));
        }
    }
}