﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sibers.BussinessLogic.DTO;
using Sibers.BussinessLogic.Interfaces;
using Sibers.DAL.Interfaces;
using Sibers.DAL.Models;

namespace Sibers.BussinessLogic.Services
{
    public class ProjectService : IProjectService
    {
        private IUnitOfWork Database { get; set; }
        private IProjectMap ProjectMap { get; set; }
        private IEmployeeMap EmployeeMap { get; set; }

        public ProjectService (IUnitOfWork uow, IProjectMap pm, IEmployeeMap em)
        {
            Database = uow;
            ProjectMap = pm;
            EmployeeMap = em;
        }

        public ProjectDTO Create (ProjectDTO projectDTO)
        {
            var newProject = ProjectMap.GetProjectDAL(projectDTO);
            newProject.ProjectChief = Database.Employers.Get(projectDTO.ProjectChiefId);
            var newProjectDAL = Database.Projects.Create(newProject);

            Database.Save();

            return ProjectMap.GetProjectDTO(newProjectDAL);
        }

        public ProjectDTO Update (ProjectDTO projectDTO)
        {
            var updatedProject = ProjectMap.GetProjectDAL(projectDTO);
            updatedProject.Id = projectDTO.Id;

            var projectForUpdate = Database.Projects.Get(projectDTO.Id);

            projectForUpdate = updatedProject;
            var updatedProjectDAL = Database.Projects.Update(projectForUpdate);
            Database.Save();

            return ProjectMap.GetProjectDTO(updatedProjectDAL);
        }

        public void Delete (ProjectDTO projectDTO)
        {
            Database.Projects.Delete(projectDTO.Id);
            Database.Save();
        }

        public void AddWorker (ProjectDTO projectDTO, EmployeeDTO employeeDTO)
        {
            var project = Database.Projects.Get(projectDTO.Id);

            if (project != null)
            {
                var employee = Database.Employers.Get(employeeDTO.Id);

                if (employee != null)
                {
                    project.ProjectWorkers.Add(employee);
                    Database.Save();
                }
            }

        }

        public void DeleteWorker (ProjectDTO projectDTO, EmployeeDTO employeeDTO)
        {
            var project = Database.Projects.Get(projectDTO.Id);

            if (project != null)
            {
                var employee = Database.Employers.Get(employeeDTO.Id);

                if (employee != null)
                {
                    project.ProjectWorkers.Remove(employee);
                    Database.Save();
                }
            }
        }

        public void Dispose ()
        {
            Database.Dispose();
        }

        public void SetChief (ProjectDTO projectDTO, EmployeeDTO employeeDTO)
        {
            var project = Database.Projects.Get(projectDTO.Id);

            if (project != null)
            {
                var employee = Database.Employers.Get(employeeDTO.Id);

                if (employee != null)
                {
                    project.ProjectChief = employee;
                    Database.Save();
                }
            }
        }

        public IEnumerable<ProjectDTO> GetAll ()
        {
            return Database.Projects.GetAll().Select(x => (ProjectMap.GetProjectDTO(x)));
        }

        public ProjectDTO Get (int id)
        {
            return ProjectMap.GetProjectDTO(Database.Projects.Get(id));
        }
    }
}