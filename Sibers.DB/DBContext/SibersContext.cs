﻿using Sibers.DAL.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Sibers.DAL.DBContext
{
    public class SibersContext : DbContext
    {
        private static readonly string connectionString = "name=SibersContext";

        public SibersContext() : base(connectionString)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SibersContext, Migrations.Configuration>());
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Employee> Employers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Project>()
                   .HasMany(p => p.ProjectWorkers)
                   .WithMany(e => e.Projects)
                   .Map(pe =>
                   {
                       pe.MapLeftKey("ProjectRefId");
                       pe.MapRightKey("EmployeeRefId");
                       pe.ToTable("ProjectEmployee");
                   });
        }
    }


}