﻿using Sibers.DAL.DBContext;
using Sibers.DAL.Interfaces;
using Sibers.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Sibers.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private SibersContext db { get; set; }

        public ProjectRepository(SibersContext context)
        {
            db = context;
        }

        public Project Create(Project item)
        {
            db.Projects.Add(item);
            return item;
        }

        public void Delete(int id)
        {
            Project project = db.Projects.Find(id);
            if (project != null)
            {
                db.Projects.Remove(project);
            }
        }

        public IEnumerable<Project> Find(Func<Project, bool> predicate)
        {
            return db.Projects.Where(predicate).ToList();
        }

        public Project Get(int id)
        {
            return db.Projects.Find(id);
        }

        public IEnumerable<Project> GetAll()
        {
            return db.Projects.ToList();
        }

        public Project Update(Project item)
        {
            db.Entry(item).State = EntityState.Modified;
            return item;
        }
    }
}