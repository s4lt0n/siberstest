﻿using Sibers.DAL.DBContext;
using Sibers.DAL.Interfaces;
using Sibers.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Sibers.DAL.Repositories
{
    public class EmployeeRepository : IRepository<Employee>
    {
        private SibersContext db { get; set; }

        public EmployeeRepository (SibersContext context)
        {
            db = context;
        }

        public Employee Create (Employee item)
        {
            db.Employers.Add(item);
            return item;
        }

        public void Delete (int id)
        {
            Employee employee = db.Employers.Find(id);
            if (employee != null)
            {
                db.Employers.Remove(employee);
            }
        }

        public IEnumerable<Employee> Find (Func<Employee, bool> predicate)
        {
            return db.Employers.Where(predicate).ToList();
        }

        public Employee Get (int id)
        {
            return db.Employers.Find(id);
        }

        public IEnumerable<Employee> GetAll ()
        {
            return db.Employers.ToList();
        }

        public Employee Update (Employee item)
        {
            db.Entry(item).State = EntityState.Modified;
            return item;
        }
    }
}