﻿using Sibers.DAL.DBContext;
using Sibers.DAL.Interfaces;
using Sibers.DAL.Models;
using System;

namespace Sibers.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private SibersContext db { get; set; }
        private ProjectRepository projectRepository { get; set; }
        private EmployeeRepository employeeRepository { get; set; }

        private bool disposed = false;

        public EFUnitOfWork()
        {
            db = new SibersContext();
        }

        public IRepository<Project> Projects
        {
            get
            {
                if (projectRepository == null)
                {
                    projectRepository = new ProjectRepository(db);
                }
                return projectRepository;
            }
        }

        public IRepository<Employee> Employers
        {
            get
            {
                if (employeeRepository == null)
                {
                    employeeRepository = new EmployeeRepository(db);
                }
                return employeeRepository;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}