﻿namespace Sibers.DAL.Models
{
    public interface IEntity
    {
        /// <summary>
        /// ID сущности, он же Primary Key
        /// </summary>
        int Id { get; set; }
    }
}
