﻿using System;
using System.Collections.Generic;

namespace Sibers.DAL.Models
{
    public class Employee : IEntity
    {
        public Employee()
        {
            this.Projects = new HashSet<Project>();
        }

        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronymic { get; set; }

        /// <summary>
        /// E-Mail
        /// </summary>
        public string EMail { get; set; }

        /// <summary>
        /// Проекты, в которых учавствует работник
        /// </summary>
        public virtual ICollection<Project> Projects { get; set; }
    }
}