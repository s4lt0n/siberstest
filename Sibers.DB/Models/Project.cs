﻿using System;
using System.Collections.Generic;

namespace Sibers.DAL.Models
{
    public class Project : IEntity
    {
        public Project()
        {
            this.ProjectWorkers = new HashSet<Employee>();
        }

        public int Id { get; set; }

        /// <summary>
        /// Название проекта
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Название компании-заказчика
        /// </summary>
        public string CustomerCompanyName { get; set; }

        /// <summary>
        /// Название компании-исполнителя
        /// </summary>
        public string ExecutingCompanyName { get; set; }

        /// <summary>
        /// Руководитель проекта
        /// </summary>
        public virtual Employee ProjectChief { get; set; }

        /// <summary>
        /// Исполнители проекта
        /// </summary>
        public virtual ICollection<Employee> ProjectWorkers { get; set; }

        /// <summary>
        /// Дата начала проекта
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата завершения проекта
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Приоритет проекта (целочисленное значение >=0)
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Текстовый комментарий
        /// </summary>
        public string Comment { get; set; }
    }
}