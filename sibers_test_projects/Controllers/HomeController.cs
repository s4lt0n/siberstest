﻿using System.Web.Mvc;
using Sibers.BussinessLogic.Interfaces;

namespace Sibers.ApplicationWeb.Controllers
{
    public class HomeController : Controller
    {
        private IProjectService projectService { get; set; }

        public HomeController (IProjectService ps)
        {
            projectService = ps;
        }

        public ActionResult Index ()
        {
            // var empls = Database.Employers.GetAll();
            //  var prjcts = Database.Projects.GetAll();

            return View();
        }

        public ActionResult Test1 ()
        {
            var asd = projectService.GetAll();
            return null;
        }

        public ActionResult About ()
        {
            ViewBag.Message = "XD";

            return View();
        }

        public ActionResult Contact ()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Contact13337 ()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}