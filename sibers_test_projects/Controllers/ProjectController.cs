﻿using Sibers.ApplicationWeb.Models.Project;
using Sibers.BussinessLogic.DTO;
using Sibers.BussinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Sibers.ApplicationWeb.Controllers
{
    public class ProjectController : Controller
    {
        IProjectService projectService { get; set; }
        IEmployeeService employeeService { get; set; }

        public ProjectController(IProjectService ps, IEmployeeService es)
        {
            projectService = ps;
            employeeService = es;
        }

        public ActionResult Index()
        {
            return View("ViewAll");
        }

        public ActionResult AllProjects(string sortOrder)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "name";
            ViewBag.StartDateSortParam = sortOrder == "date" ? "date_desc" : "date";
            ViewBag.PrioritySortParam = sortOrder == "prior" ? "prior_desc" : "prior";
            var asd = projectService.GetAll().ToList();
            var projects = projectService.GetAll().Select(x =>
                new ProjectViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    CustomerCompanyName = x.CustomerCompanyName,
                    ExecutingCompanyName = x.ExecutingCompanyName,
                    Comment = x.Comment,
                    ProjectChiefName = employeeService.Get(x.ProjectChiefId)?.FullName,
                    Priority = x.Priority,
                    Workers = x.ProjectWorkersId.Select(w => employeeService.Get(w)?.FullName),
                    StartDate = x.StartDate,
                    EndDate = x.EndDate
                });
            switch (sortOrder)
            {
                case "name_desc":
                    projects = projects.OrderByDescending(s => s.Name);
                    break;
                case "date":
                    projects = projects.OrderBy(s => s.StartDate);
                    break;
                case "date_desc":
                    projects = projects.OrderByDescending(s => s.StartDate);
                    break;
                case "prior":
                    projects = projects.OrderBy(s => s.Priority);
                    break;
                case "prior_desc":
                    projects = projects.OrderByDescending(s => s.Priority);
                    break;
                default:
                    break;
            }

            return PartialView(projects.ToList());
        }

        public ActionResult Create()
        {
            var model = new ProjectCreateModel
            {
                ProjectChiefs = GetAllEmployers(),
                ProjectWorkers = GetAllEmployers()
            };
            return View(model);
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(ProjectCreateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // TODO: Add insert logic here
                    var workers = model.SelectedWorkerIds
                         .Select(x => employeeService.Get(x));

                    var newProject = new ProjectDTO
                    {
                        Name = model.Name,
                        CustomerCompanyName = model.CustomerCompanyName,
                        ExecutingCompanyName = model.ExecutingCompanyName,
                        StartDate = model.StartDate,
                        EndDate = model.EndDate,
                        Comment = model.Comment,
                        Priority = model.Priority,
                        ProjectWorkersId = model.SelectedWorkerIds,
                        ProjectChiefId = model.SelectedChiefId
                    };

                    var createdProject = projectService.Create(newProject);

                    projectService.SetChief(createdProject, employeeService.Get(model.SelectedChiefId));

                    foreach (var worker in workers)
                    {
                        projectService.AddWorker(createdProject, worker);
                    }

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    return new JsonResult() { Data = new { error = ex.Message } };
                }
            }
            else
            {
                model.ProjectChiefs = GetAllEmployers();
                model.ProjectWorkers = GetAllEmployers();

                return View(model);
            }

        }

        private IEnumerable<SelectListItem> GetAllEmployers()
        {
            var allEmployers = employeeService.GetAll()
                .Select(t => new SelectListItem { Value = t.Id.ToString(), Text = t.FullName });
            return allEmployers;
        }

    }
}