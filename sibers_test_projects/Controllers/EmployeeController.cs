﻿using Sibers.ApplicationWeb.Models.Employee;
using Sibers.BussinessLogic.DTO;
using Sibers.BussinessLogic.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Sibers.ApplicationWeb.Controllers
{
    public class EmployeeController : Controller
    {
        IEmployeeService employeeService { get; set; }

        public EmployeeController(IEmployeeService es)
        {
            employeeService = es;
        }

        public ActionResult Index()
        {
            return View("ViewAll");
        }

        public ActionResult AllEmployers(string sortOrder, string currentFilter, string strSearch, int? page)
        {
            var employers = employeeService.GetAll().Select(x =>
            new EmployeeViewModel
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Patronymic = x.Patronymic,
                EMail = x.EMail
            });
            return PartialView(employers.ToList());
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(EmployeeCreateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // TODO: Add insert logic here
                    var newEmployee = new EmployeeDTO
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Patronymic = model.Patronymic,
                        EMail = model.EMail
                    };
                    employeeService.Create(newEmployee);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View();
                }
            }
            else
            {
                return View(model);
            }

        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            var model = employeeService.Get(id);
            var modelEdit = new EmployeeCreateModel
            {
                Id = model.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Patronymic = model.Patronymic,
                EMail = model.EMail
            };
            return View(modelEdit);
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(EmployeeCreateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // TODO: Add update logic here
                    var modelToUpdate = employeeService.Get(model.Id);
                    modelToUpdate.FirstName = model.FirstName;
                    modelToUpdate.LastName = model.LastName;
                    modelToUpdate.Patronymic = model.Patronymic;
                    modelToUpdate.EMail = model.EMail;
                    employeeService.Update(modelToUpdate);

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    return View();
                }
            }
            return View(model);
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
