﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sibers.ApplicationWeb.Models.Employee
{
    public class EmployeeCreateModel
    {
        public int Id { get; set; }
        /// <summary>
        /// Имя
        /// </summary>

        [Required]
        [DisplayName("Имя")]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>

        [Required]
        [DisplayName("Фамилия")]
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>

        [DisplayName("Отчество")]
        public string Patronymic { get; set; }

        /// <summary>
        /// E-Mail
        /// </summary>

        [Required]
        [DisplayName("Электронная почта")]
        [EmailAddress(ErrorMessage = "НОМРАЛЬНО ВВЕДИ")]
        public string EMail { get; set; }
    }
}