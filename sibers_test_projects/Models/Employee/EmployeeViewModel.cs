﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Sibers.ApplicationWeb.Models.Employee
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }

        [DisplayName("Имя")]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>

        [DisplayName("Фамилия")]
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>

        [DisplayName("Отчество")]
        public string Patronymic { get; set; }

        /// <summary>
        /// E-Mail
        /// </summary>

        [DisplayName("Электронная почта")]
        public string EMail { get; set; }
    }
}