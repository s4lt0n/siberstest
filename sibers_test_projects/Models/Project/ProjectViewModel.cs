﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Sibers.ApplicationWeb.Models.Project
{
    public class ProjectViewModel
    {
        public int Id { get; set; }

        /// <summary>
        /// Название проекта
        /// </summary>
        [DisplayName("Проект")]
        public string Name { get; set; }

        /// <summary>
        /// Название компании-заказчика
        /// </summary>
        [DisplayName("Заказчик")]
        public string CustomerCompanyName { get; set; }

        /// <summary>
        /// Название компании-исполнителя
        /// </summary>
        [DisplayName("Исполнитель")]
        public string ExecutingCompanyName { get; set; }

        /// <summary>
        /// Руководитель проекта
        /// </summary>
        [DisplayName("Руководитель проекта")]
        public string ProjectChiefName { get; set; }

        /// <summary>
        /// Дата начала проекта
        /// </summary>
        [DisplayName("Дата начала проекта")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата завершения проекта
        /// </summary>
        [DisplayName("Дата ожидаемого завершения")]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Приоритет проекта (целочисленное значение >=0)
        /// </summary>
        [DisplayName("Приоритет")]
        public int Priority { get; set; }

        /// <summary>
        /// Текстовый комментарий
        /// </summary>
        [DisplayName("Комментарий")]
        public string Comment { get; set; }

        /// <summary>
        /// Исполнители проекта
        /// </summary>
        [DisplayName("Исполнители (работники)")]
        public IEnumerable<string> Workers { get; set; }

    }
}