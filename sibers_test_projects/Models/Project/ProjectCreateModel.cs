﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sibers.ApplicationWeb.Models.Project
{
    public class ProjectCreateModel
    {
        /// <summary>
        /// Название проекта
        /// </summary>

        [Required]
        [DisplayName("Проект")]
        public string Name { get; set; }

        /// <summary>
        /// Название компании-заказчика
        /// </summary>

        [Required]
        [DisplayName("Заказчик")]
        public string CustomerCompanyName { get; set; }

        /// <summary>
        /// Название компании-исполнителя
        /// </summary>

        [Required]
        [DisplayName("Исполнитель")]
        public string ExecutingCompanyName { get; set; }


        [Required]
        [DisplayName("Руководитель проекта")]
        public int SelectedChiefId { get; set; }

        /// <summary>
        /// Руководитель проекта
        /// </summary>
        [DisplayName("Руководитель проекта")]
        public IEnumerable<SelectListItem> ProjectChiefs { get; set; }

        /// <summary>
        /// Дата начала проекта
        /// </summary>

        [Required]
        [DisplayName("Дата начала проекта")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата завершения проекта
        /// </summary>

        [Required]
        [DisplayName("Дата ожидаемого завершения")]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Приоритет проекта (целочисленное значение >=0)
        /// </summary>

        [Required]
        [DisplayName("Приоритет")]
        public int Priority { get; set; }

        /// <summary>
        /// Текстовый комментарий
        /// </summary>
        [DisplayName("Комментарий")]
        public string Comment { get; set; }

        /// <summary>
        /// Исполнители проекта
        /// </summary>
        public IEnumerable<SelectListItem> ProjectWorkers { get; set; }

        /// <summary>
        /// Исполнители проекта
        /// </summary>
        public SelectListItem SelectedProjectWorkers { get; set; }

        [Required]
        [MinLength(1)]
        [DisplayName("Исполнители (работники)")]
        public int[] SelectedWorkerIds { get; set; }
    }
}